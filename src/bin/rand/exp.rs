use rand_distr::Exp;
use clap::ArgMatches;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, exponential_matches: &ArgMatches) {
	let lambda: f64 = if exponential_matches.contains_id("lambda") {
		*exponential_matches
			.get_one::<f64>("lambda")
			.expect("")
	} else {
		1.0_f64 / *exponential_matches
			.get_one::<f64>("scale")
			.expect("")
	};

	// TODO: optimize with Exp1 on default
	let dist = Exp::new(lambda).unwrap();

	print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
}
