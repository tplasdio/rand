use rand_distr::Uniform;
use clap::ArgMatches;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, uniform_matches: &ArgMatches) {
	let a = uniform_matches
		.get_one::<f64>("alpha")
		.expect("");

	let b = uniform_matches
		.get_one::<f64>("beta")
		.expect("");

	let dist = Uniform::new(a,b);

	print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
}
