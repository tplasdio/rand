use rand_distr::Binomial;
use clap::ArgMatches;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, matches: &ArgMatches) {
	let success: f64 = if matches.contains_id("success") {
		*matches
			.get_one::<f64>("success")
			.expect("")
	} else if matches.contains_id("failure") {
		1.0_f64 - (*matches
			.get_one::<f64>("failure")
			.expect(""))
	} else {
		0.5_f64
	};

	let trials = *matches
		.get_one::<u64>("trials")
		.expect("");

	let dist = Binomial::new(trials,success).unwrap();

	print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
}
