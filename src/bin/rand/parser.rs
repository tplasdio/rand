use rand::{SeedableRng, RngCore};
use rand::rngs::StdRng;

use clap::{
	arg,
	value_parser,
	Command,
	ArgGroup,
	ArgAction,
	ArgMatches,
};

pub fn cli() -> Command {
	Command::new("rand")
	.about("Print random numbers from statistical distributions")
	.arg(arg!(-n --samples <n> "Number of samples to print")
		.default_value("1")
		.ignore_case(true)
		.value_parser(value_parser!(u128)))
	.arg(arg!(-s --scale <s> "Number of decimals to show")
		.default_value("2")
		.ignore_case(true)
		.value_parser(value_parser!(u64).range(0..=49)))
	.arg(arg!(--seed <seed> "Number to initialize the pseudorandom number generator. Random by default")
		.ignore_case(true)
		.value_parser(value_parser!(u64)))

	.arg(arg!(-d --ors <d> "Separator of output lines")
		.default_value("\n")
		.conflicts_with("null"))
	.arg(arg!(-z --null "Separate output lines with NUL")
		.visible_short_alias('0')
		.visible_alias("print0")
		.action(ArgAction::SetTrue)
		.conflicts_with("ors"))
	.arg(arg!(--eof <eof> "Last printed character")
		.default_value("\n"))

	.subcommand(
		Command::new("uniform")
		.short_flag('U')
		.long_flag("uniform")
		.about("Uniform distribution")
		.visible_alias("U")
		.arg(arg!(-a --alpha <alpha> "Lower bound")
			.visible_short_alias('α')
			.default_value("0.0")
			.value_parser(value_parser!(f64)))
		.arg(arg!(-b --beta <beta> "Upper bound")
			.visible_short_alias('β')
			.default_value("1.0")
			.value_parser(value_parser!(f64)))
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("normal")
		.short_flag('N')
		.long_flag("normal")
		.about("Normal distribution")
		.visible_alias("N")
		.arg(arg!(-m --mean <m> "Mean/Median/Mode")
			.visible_short_alias('μ')
			.default_value("0.0")
			.value_parser(value_parser!(f64)))
		.arg(arg!(-s --stdev <s> "Standard deviation")
			.visible_short_alias('σ')
			.value_parser(value_parser!(f64))
			.conflicts_with("variance"))
		.arg(arg!(-v --variance <v> "Variance")
			.visible_alias("σ2")
			.value_parser(value_parser!(f64))
			.conflicts_with("stdev"))
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("tstudent")
		.short_flag('T')
		.long_flag("t-student")
		.about("T-student distribution")
		.visible_aliases(["T", "studentt"])
		.arg(arg!(-m --mean <m> "Mean/Median/Mode")
			.visible_short_alias('μ')
			.default_value("0.0")
			.value_parser(value_parser!(f32)))
		.arg(arg!(-s --stdev <s> "Standard deviation")
			.visible_short_alias('σ')
			.value_parser(value_parser!(f32))
			.conflicts_with("variance"))
		.arg(arg!(-v --variance <v> "Variance")
			.visible_alias("σ2")
			.value_parser(value_parser!(f32))
			.conflicts_with("stdev"))
		.arg(arg!(-d --"degrees" <d> "Degrees of freedom")
			.visible_short_alias('ν')
			.visible_alias("df")
			.default_value("1")
			.value_parser(value_parser!(f32)))
			// FIXME: validate float > 0
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("exponential")
		.short_flag('E')
		.long_flag("exponential")
		.about("Exponential distribution")
		.visible_aliases(["E","Exp"])
		.group(
			ArgGroup::new("parameters")
			.required(true)
			.args(["lambda", "scale"]),
		)
		.arg(arg!(-l --lambda <l> "Rate parameter")
			.visible_short_alias('λ')
			.default_value("1.0")
			.value_parser(value_parser!(f64)))
		// TODO: validate [0,inf>
		.arg(arg!(-s --scale <s> "Scale parameter")
			.visible_short_alias('σ')
			.default_value("1.0")
			.value_parser(value_parser!(f64)))
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("chisquared")
		.short_flag('C')
		.long_flag("chisquared")
		.about("Chi distribution")
		.visible_aliases(["X","X2","X²","Chi","Chi2"])
		.aliases(["Χ","Χ²","Χ2","χ","χ²","χ2"])
		.arg(arg!(-d --degrees <d> "Degrees of freedom")
			.visible_short_alias('k')
			.visible_alias("df")
			.default_value("1")
			.value_parser(value_parser!(f32)))
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("gamma")
		.short_flag('G')
		.long_flag("gamma")
		.about("Gamma distribution")
		.visible_aliases(["G","Γ"])
		.group(
			ArgGroup::new("parameters")
			.args(["scale", "rate"]),
		)
		.arg(arg!(-k --shape <shape> "Shape parameter")
			.visible_short_aliases(['α','a'])
			.visible_alias("alpha")
			.default_value("1.0")
			.value_parser(value_parser!(f64)))
		.arg(arg!(--scale <scale> "Scale parameter")
			.visible_short_aliases(['θ','t'])
			.visible_alias("theta")
			.default_value("1.0")
			.value_parser(value_parser!(f64)))
		.arg(arg!(--rate <rate> "Rate parameter")
			.visible_short_aliases(['β','b'])
			.visible_alias("beta")
			.default_value("1.0")
			.value_parser(value_parser!(f64)))
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("poisson")
		.short_flag('P')
		.long_flag("poisson")
		.visible_aliases(["P","Pois"])
		.about("Poisson distribution")
		.arg(arg!(-l --lambda <lambda> "Lambda parameter")
			.visible_short_aliases(['λ','μ','σ'])
			.visible_aliases(["mean", "variance","stdev","σ2"])
			.default_value("1.0")
			.value_parser(value_parser!(f64)))
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("binomial")
		.short_flag('B')
		.long_flag("binomial")
		.visible_aliases(["B","bernoulli"])
		.about("Binomial/Bernoulli distribution")
		.group(
			ArgGroup::new("parameters")
			.args(["success", "failure"]),
		)
		// Maybe TODO: when -N passed, fall back to hypergeometric, with warning
		// TODO: validate [0,1]
		.arg(arg!(-p --success <success> "Probability of success")
			.visible_short_aliases(['s'])
			.value_parser(value_parser!(f64)))
		.arg(arg!(-q --failure <failure> "Probability of failure")
			.visible_short_aliases(['f'])
			.value_parser(value_parser!(f64)))
		.arg(arg!(-n --trials <trials> "Number of trials")
			.visible_short_aliases(['t'])
			.default_value("1")
			.value_parser(value_parser!(u64)))
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("geometric")
		.short_flag('g')
		.long_flag("geometric")
		.visible_aliases(["Geo","g"])
		.about("Geometric distribution")
		.group(
			ArgGroup::new("parameters")
			.args(["success", "failure"]),
		)
		// TODO: validate [0,1]
		.arg(arg!(-p --success <success> "Probability of success")
			.visible_short_aliases(['s'])
			.value_parser(value_parser!(f64)))
		.arg(arg!(-q --failure <failure> "Probability of failure")
			.visible_short_aliases(['f'])
			.value_parser(value_parser!(f64)))
		.arg(arg!(-'1' --shifted "Use shifted distribution, starting on 1")
			 .action(ArgAction::SetTrue))
		.infer_long_args(true)
	)

	.subcommand(
		Command::new("hypergeometric")
		.short_flag('H')
		.long_flag("hypergeometric")
		.visible_aliases(["H","hyper","hypergeo"])
		.about("Hypergeometric distribution")
		.group(
			ArgGroup::new("parameters")
			.args(["sstates", "fstates","success","failure"]),
		)
		// Maybe TODO: when -N not passed or infty,∞, fall back to binomial, with warning
		.arg(arg!(-'N' --population <N> "Population size")
			.default_value("10")
			.value_parser(value_parser!(u64)))
		// TODO: validate K,F < N
		.arg(arg!(-'K' --sstates <K> "Number of success states")
			.visible_alias("success-states")
			.value_parser(value_parser!(u64)))
		.arg(arg!(-'F' --fstates <F> "Number of failure states")
			 .visible_alias("failure-states")
			 .value_parser(value_parser!(u64)))
		// TODO: validate p,q in [0,1]
		.arg(arg!(-p --success <success> "Proportion of success states to population size")
			.visible_short_alias('s')
			.value_parser(value_parser!(f64)))
		.arg(arg!(-q --failure <failure> "Proportion of failure states to population size")
			.visible_short_alias('f')
			.value_parser(value_parser!(f64)))
		.arg(arg!(-n --draws <n> "Number of draws")
			.visible_short_alias('d')
			.default_value("1")
			.value_parser(value_parser!(u64)))
		.infer_long_args(true)
	)

	.subcommand_required(true)
	.infer_subcommands(true)
	.infer_long_args(true)
}

pub struct GlobalArgs<'a> {
	pub n: &'a u128,
	pub scale: usize,
	pub ors: &'a str,
	pub eof: &'a String,
	pub rng: Box<dyn RngCore>,
}

fn get_rng(
	matches: &ArgMatches
) -> Box<dyn RngCore> {
	if matches.contains_id("seed") {
		let seed = *matches
			.get_one::<u64>("seed")
			.expect("")
		;
		Box::new(StdRng::seed_from_u64(seed))
	} else {
		Box::new(rand::thread_rng())
	}
}

pub fn parse_global_args(matches: &ArgMatches) -> GlobalArgs {
	GlobalArgs {
		n: &*matches
			.get_one::<u128>("samples")
			.expect(""),
		scale: *matches
			.get_one::<u64>("scale")
			.expect("") as usize,
		ors: if matches.get_flag("null") {
				"\0"
			} else {
				matches
					.get_one::<String>("ors")
					.expect("")
			},
		eof: matches
				.get_one::<String>("eof")
				.expect(""),
		rng: get_rng(&matches),
	}
}
