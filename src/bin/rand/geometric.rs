use rand_distr::Geometric;
use clap::ArgMatches;
use rand_distr::Distribution;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, matches: &ArgMatches) {
	let success: f64 = if matches.contains_id("success") {
		*matches
			.get_one::<f64>("success")
			.expect("")
	} else if matches.contains_id("failure") {
		1.0_f64 - *matches
			.get_one::<f64>("failure")
			.expect("")
	} else {
		0.5_f64
	};

	// TODO: optimize with StandardGeometric on p=0.5
	let dist = Geometric::new(success).unwrap();

	if matches.get_flag("shifted") {
		for _ in 1..*g.n {
			print!("{:.*}{}", g.scale, dist.sample(&mut g.rng) + 1, g.ors);
		}
		print!("{:.*}{}", g.scale, dist.sample(&mut g.rng) + 1, g.eof);
	} else {
		print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
	}
}
