use rand_distr::Hypergeometric;
use clap::ArgMatches;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, matches: &ArgMatches) {
	let population = *matches
		.get_one::<u64>("population")
		.expect("");

	let sstates: u64 = if matches.contains_id("sstates") {
		*matches
			.get_one::<u64>("sstates")
			.expect("")
	} else if matches.contains_id("fstates") {
		population - (*matches
			.get_one::<u64>("fstates")
			.expect(""))
	} else if matches.contains_id("success") {
		( (population as f64) * (*matches
			.get_one::<f64>("success")
			.expect("")) ) as u64
	} else if matches.contains_id("failure") {
		( (population as f64) * (1.0_f64 - *matches
			.get_one::<f64>("success")
			.expect("")) ) as u64
	} else {
		population / 2_u64  // Optimize with default pop in a const
	};

	let draws = *matches
		.get_one::<u64>("draws")
		.expect("");

	let dist = Hypergeometric::new(population,sstates,draws).unwrap();

	print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
}
