use rand_distr::Gamma;
use clap::ArgMatches;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, matches: &ArgMatches) {
	let shape = *matches
		.get_one::<f64>("shape")
		.expect("");

	let theta: f64 = if matches.contains_id("scale") {
		*matches
			.get_one::<f64>("scale")
			.expect("")
	} else {
		1.0_f64 / *matches
			.get_one::<f64>("rate")
			.expect("")
	};

	let dist = Gamma::new(shape, theta).unwrap();

	print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
}
