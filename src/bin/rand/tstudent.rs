use rand_distr::{
	Distribution,
	StudentT,
};
use clap::ArgMatches;

use crate::GlobalArgs;

pub fn print(g: &mut GlobalArgs, tstudent_matches: &ArgMatches) {
	let degrees = *tstudent_matches
		.get_one::<f32>("degrees")
		.expect("");

	let mean = *tstudent_matches
		.get_one::<f32>("mean")
		.expect("");

	let stdev: f32 = if tstudent_matches.contains_id("stdev") {
		*tstudent_matches
			.get_one::<f32>("stdev")
			.expect("")
	} else if tstudent_matches.contains_id("variance") {
		(*tstudent_matches
			.get_one::<f32>("variance")
			.expect("")
		).sqrt()
	} else {
		1.0_f32
	};

	let dist = StudentT::new(degrees).unwrap();

	for _ in 1..*g.n {
		print!(
			"{:.*}{}", g.scale, dist.sample(&mut g.rng) * stdev + mean, g.ors
		);
	}
	// TODO: optimize by not operating when default
	print!("{:.*}{}", g.scale, dist.sample(&mut g.rng) * stdev + mean, g.eof);
}
