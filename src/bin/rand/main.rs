/*!
rand is a command-line random number generator from statistical distributions
*/

mod parser;
mod printloop;
mod uniform;
mod normal;
mod exp;
mod tstudent;
mod chisquared;
mod gamma;
mod poisson;
mod binomial;
mod geometric;
mod hypergeometric;

use parser::{
	cli,
	parse_global_args,
	GlobalArgs,
};

use printloop::print_samples;

fn main() {

	let matches = cli().get_matches();

	let mut global_args: GlobalArgs = parse_global_args(&matches);

	// TODO: on discrete distributions, indicate that scale won't do
	// anything. Maybe just by default, put add that it can print decimals anymays
	match matches.subcommand() {
		Some(("uniform", matches)) => uniform::print(&mut global_args, &matches),
		Some(("normal", matches)) => normal::print(&mut global_args, &matches),
		Some(("tstudent", matches)) => tstudent::print(&mut global_args, &matches),
		Some(("exponential", matches)) => exp::print(&mut global_args, &matches),
		Some(("chisquared", matches)) => chisquared::print(&mut global_args, &matches),
		Some(("gamma", matches)) => gamma::print(&mut global_args, &matches),
		Some(("poisson", matches)) => poisson::print(&mut global_args, &matches),
		Some(("binomial", matches)) => binomial::print(&mut global_args, &matches),
		Some(("geometric", matches)) => geometric::print(&mut global_args, &matches),
		Some(("hypergeometric", matches)) => hypergeometric::print(&mut global_args, &matches),
		_ => unreachable!(),
	}

}
