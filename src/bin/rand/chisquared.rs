use rand_distr::ChiSquared;
use clap::ArgMatches;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, matches: &ArgMatches) {
	let degrees = *matches
		.get_one::<f32>("degrees")
		.expect("");

	let dist = ChiSquared::new(degrees).unwrap();

	print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
}
