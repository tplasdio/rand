use rand_distr::Poisson;
use clap::ArgMatches;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, matches: &ArgMatches) {
	let lambda = *matches
		.get_one::<f64>("lambda")
		.expect("");

	let dist = Poisson::new(lambda).unwrap();

	print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
}
