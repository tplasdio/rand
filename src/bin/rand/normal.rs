use rand_distr::Normal;
use clap::ArgMatches;

use crate::GlobalArgs;
use crate::print_samples;

pub fn print(g: &mut GlobalArgs, normal_matches: &ArgMatches) {
	let mean = *normal_matches
		.get_one::<f64>("mean")
		.expect("");

	let stdev: f64 = if normal_matches.contains_id("stdev") {
		*normal_matches
			.get_one::<f64>("stdev")
			.expect("")
	} else if normal_matches.contains_id("variance") {
		(*normal_matches
			.get_one::<f64>("variance")
			.expect("")
		).sqrt()
	} else {
		1.0_f64
	};

	// TODO: optimize with StandardNormal on default
	let dist = Normal::new(mean,stdev).unwrap();

	print_samples(g.n, &g.scale, &dist, &mut g.rng, g.ors, g.eof);
}
