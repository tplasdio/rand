use rand_distr::Distribution;
use rand::RngCore;

pub fn print_samples<T1, T2, T3>(
	n: &u128,
	scale: &usize,
	dist: &T2,
	rng: &mut T3,
	ors: &str,
	eof: &String,
)
	where
		T1: std::fmt::Display,
		T2: Distribution<T1>,
		T3: RngCore,
{
	for _ in 1..*n {
		print!(
			"{:.*}{}", scale, dist.sample(rng), ors
		);
	}
	print!("{:.*}{}", scale, dist.sample(rng), eof);
}
