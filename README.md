<p align="center">
  <h1 align="center">Rand</h1>
  <p align="center">
    A command-line random number generator from statistical distributions
  </p>
</p>

## Table of Contents

- [📟 Examples](#examples)
- [✨ Features](#features)
- [🚀 Installation](#installation)
- [💭 See also](#see-also)
- [📝 License](#license)

## 📟 Examples

```sh
rand uniform              # Print random float from ~U[0,1>
rand U                    # Print random float from ~U[0,1>
rand -s=5 U               # Print random float with 5 decimals from ~U[0,1>
rand -n=10 U              # Print 10 random floats from ~U[0,1>
rand -n=10 --seed=5 U     # Print using a seed for reproducibility
rand U -a=3 -b=7          # Print random float from ~U[3,7>
rand -n=100 -s=0 U -b=30  # Print 100 random integers from 1 to 30
rand N                    # Print random float from ~N(0,1)
rand N -m=4 -s=3          # Print random float from ~N(4,3²)
rand N -m=4 -v=9          # Print random float from ~N(4,9)
rand T -m=4 -v=9 --df=10  # Print random float from ~t-student_ν=10(4,9)
rand E -l=0.5             # Print random float from ~Exp(λ=0.5)
rand help                 # Show help
```

## ✨ Features

- Flexible subcommand and options' names:
  - Many abbreviations, aliases, optional use of "=", etc.
- Many distributions, with more ways to specify parameters than usual
  - [X] Uniform
  - [X] Normal
  - [X] T-student
  - [X] Exponential
  - [X] Chi-squared
  - [X] Gamma
  - [X] Poisson
  - [X] Binomial
  - [X] Geometric
  - [X] Hypergeometric

I plan to support distributions from [here](https://docs.rs/rand_distr/latest/rand_distr/).

## 🚀 Installation

<details>
<summary>From source</summary>

- *Build Dependencies*: `git`, `rust`, `cargo`

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/rand.git
cd rand
cargo install path=.
```

And add the specified path to your `PATH`

</details>

<details>
<summary>Arch Linux</summary>

```sh
curl -O https://codeberg.org/tplasdio/rand/raw/branch/main/packaging/PKGBUILD-git
makepkg -si -p PKGBUILD-git
```

*Note: The binary is named `rnd` in Arch to not conflict with AUR's `rand`,
you may modify the PKGBUILD before installing*

</details>

<details>
<summary>Other Linux</summary>

Install [lure](https://gitea.elara.ws/Elara6331/lure/releases/latest)

```sh
curl -O https://codeberg.org/tplasdio/rand/raw/branch/main/packaging/lure-git.sh
lure build -s lure-git.sh
```

- Debian, Ubuntu or other .deb distros: `dpkg -i rand*.deb`
- Fedora, OpenSUSE or other .rpm distros: `rpm -i rand*.rpm`
- Alpine Linux: `apk add --allow-untrusted rand*.apk`

</details>

## 💭 See also

Complementary or similar:
- [shuf](https://git.savannah.gnu.org/cgit/coreutils.git/tree/src/shuf.c): Command-line shuffler and random permutations generator. Part of GNU's coreutils
- [Datamash](https://www.gnu.org/software/datamash): Command-line program which performs basic numeric, textual and statistical operations on input textual data file
- [Gnuplot](http://www.gnuplot.info): Command-line and GUI plotting program that accepts input textual files
- [R](https://www.r-project.org/): Programming language for statistics, has interactive REPL and builtin random functions
- [Miller](https://github.com/johnkerl/miller): Some statistical processing on text file formats like CSV

Other CLI RNGs:
- [rand](https://sourceforge.net/projects/rand/): Fast. Positive integers or floats from 0 to 1
- [randomutils](https://gitlab.com/hdante/randomutils/): Fast; 64-bit random numbers; without replacement; dice rolls, passwords
- [num-utils/random](https://suso.suso.org/programs/num-utils/man1/random.html): With ranges and steps. Part of num-utils
- [random-rs](https://github.com/one-d-wide/random-rs)
- [ivanch/random](https://github.com/ivanch/random)

None of these draws numbers from statistical distributions other than the
Uniform (except R of course), hence why I made this tool.

## 📝 License

GPLv3-or-later
