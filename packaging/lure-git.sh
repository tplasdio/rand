name=rand-git
_name=rand
__name=rand  # ← Installation name, you may change it
description='CLI random number generator from statistical distribution'
version=0.3.0.r24.5a75965
release=1
architectures=('all')
licenses=('GPL-3.0-or-later')
maintainer="<tplasdio cat codeberg dog com>"
homepage='https://codeberg.org/tplasdio/rand'
build_deps=(
 rust
 cargo
)
build_deps_debian=(
 rustc
 cargo
)
sources=("git+$homepage")
checksums=('SKIP')

version() {
	cd "$srcdir/$_name"
	git-version
}

prepare() {
	cd "$srcdir/$_name"

	# download dependencies
	cargo fetch --locked # --target "$architecture-unknown-linux-gnu"
}

build() {
	cd "$srcdir/$_name"

	cargo build --frozen --release --all-features
}

package() {
	cd "$srcdir/$_name"

	mv target/release/{rand,$__name}

	install -vDm644 "README.md" "$pkgdir/usr/share/doc/$__name/README.md"
	install -vDm644 "LICENSE" "$pkgdir/usr/share/licenses/$__name/LICENSE"
	install -vDm755 -t "$pkgdir/usr/bin" target/release/$__name
}
